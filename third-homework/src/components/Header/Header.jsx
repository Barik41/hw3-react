import React, { useEffect, useState, memo } from "react";
import styles from "./Header.module.scss"
// import StarIcon from "../svgComponents/StarIcon";
import { ReactComponent as StarIcon} from "../../asset/star.svg"
import { ReactComponent as BasketIcon} from "../../asset/basket.svg"
import PropTypes from "prop-types";
import { NavLink} from 'react-router-dom'
import classNames from "classnames";


const Header = (props) => {

    const { goods, choosenItems } = props;

    const [basketAmount, setBasketAmount] = useState(0)

    useEffect(() => {
        let counter = 0;

        choosenItems.forEach(({count}) => {
            counter += count
        });

        setBasketAmount(counter)
    }, [choosenItems])

    let starAmount = 0;

    goods.forEach(el => {
        if(el.isFavourite) {
            starAmount++
        }
    });
 
    return(

        <header className={styles.header}>
            <span>LAPTOP Store</span>
            <div className={styles.nav}>
                <NavLink className={({ isActive }) => classNames(styles.linkPage, {[styles.isActive]: isActive})} to='/'>Home</NavLink>
                <NavLink className={({ isActive }) => classNames(styles.linkPage, {[styles.isActive]: isActive})} to='/favourite'>Favourite</NavLink>
                <NavLink className={({ isActive }) => classNames(styles.linkPage, {[styles.isActive]: isActive})} to='/choosen'>Choosen</NavLink>
            </div>
            <div className={styles.wrap}>
                <div > 
                    <StarIcon/>
                    <span>{starAmount}</span>
                </div>
                <div > 
                    <BasketIcon />
                    <span>{basketAmount}</span>
                </div>
            </div>
        </header>
    )
}

Header.propTypes = {
    goods: PropTypes.array,
    choosenItems: PropTypes.array
}

Header.defaultProps = {
    goods: [],
    choosenItems: []
}

export default memo(Header);