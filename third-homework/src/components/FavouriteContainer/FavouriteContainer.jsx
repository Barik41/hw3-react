import React, {memo} from "react";
import Card from "../Card/Card";
import PropTypes from "prop-types";

const ItemsContainer = (props) => {

    const { items, setChosenItems, toggleFavourite} = props;

    return(
        <section>
            <div className="cardWrap">
            <h2> Favourite Laptops</h2>
                {items && items.filter(item => item.isFavourite === true).map(({name, price, url, id, color, isFavourite }) => <Card key={id} name={name} price={price} url={url} id={id} color={color} isFavourite={isFavourite} setChosenItems={setChosenItems} toggleFavourite={toggleFavourite}/> )}
            </div>
        </section>
    )

}

ItemsContainer.propTypes = {
    items: PropTypes.array,
    setChosenItems: PropTypes.func,
    toggleFavourite: PropTypes.func
}

ItemsContainer.defaultProps = {
    items: [],
    setChosenItems: () => {},
    toggleFavourite: () => {}
}

export default memo(ItemsContainer);