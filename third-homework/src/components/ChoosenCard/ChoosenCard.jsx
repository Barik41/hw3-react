import React, {useState} from "react";
import styles from "./ChoosenCard.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import classNames from "classnames";
import Modal from "../Modal/Modal";
import {saveStateToLocalStorage} from "../../utils/localStorageFuncion"
import { CART_KEY } from "../../Constants"


const ChoosenCard = (props) => {
    const { url, name, price, color, count, setChosenItems} = props;

    const [header, setHeader] = useState('Do you want to delete this Laptop?');
    const [text, setText] = useState(`When you click OK, the laptop will be removed from the basket.`);
    const [modal, setModal] = useState(false);

    const clickToggle = (e) => {
        // e.preventDefault()
        if(!modal) {
          setModal(true)
        } else {
          setModal(false)
        }
    }

    const handleClickToRemove = (e) => {
        setChosenItems((prev) => {
            const newCardState = [...prev]
            const index = newCardState.findIndex((item) => item.name === name)

            // let counter = 0;

            // newCardState.forEach(({count}) => {
            //     counter += count
            // });

            // console.log(counter)
            // console.log(count)
            // console.log(prev)

            // if (counter === 0) {
            //     saveStateToLocalStorage(CART_KEY, [])
            //     return []
            // }
            console.log(index)
            if (index !== -1) {
            
                if (newCardState[index].count > 1) {
                    newCardState[index].count--
                } else {
                    newCardState.splice(index, 1)
                }
                saveStateToLocalStorage(CART_KEY, newCardState)
                clickToggle()
                return newCardState
            } else {
                const newState = [{name, price, url, color, count}, ...prev]
                saveStateToLocalStorage(CART_KEY, newState)
                clickToggle()
                return newState
            }
        })
    }

    return(
        <section>

            <div className={styles.wrapper}>
                <img className={styles.img} src={url} alt={name} />
                <p className={styles.caption}>{name}</p>
                <div className={styles.infoWrap}>
                    <span className={styles.info}>your price: {price} $</span>
                    <span className={styles.desc}> color: {color}</span>
                </div>
                <p className={styles.info}>Number: {count}</p>
                <Button onClick={clickToggle} className={classNames(styles.btn)}>Delete Laps</Button>

                {modal ? <Modal header={header} text={text} isCloseButton={clickToggle} actions={
                    <>
                        <button className='btnActions' onClick={handleClickToRemove}>Ok</button>
                        <button className='btnActions' onClick={clickToggle}>Cancel</button>
                    </>
                } closefunction={clickToggle}/> : null}

            </div>

        </section>
    )

}

ChoosenCard.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string,
    color: PropTypes.string,
    count: PropTypes.number,
}

ChoosenCard.defaultProps = {
    name: 'text',
    price: 'text',
    url: 'text',
    count: 0,
    color: 'text',
}


export default ChoosenCard;