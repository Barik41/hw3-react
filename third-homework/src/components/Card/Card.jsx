import React, { useState, memo } from "react";
import styles from './Card.module.scss'
import Button from "../Button/Button";
// import starIcon from "../../asset/star.svg"
// import StarIcon from "../svgComponents/StarIcon";
import { ReactComponent as StarIcon} from "../../asset/star.svg"
import classNames from "classnames";
import { saveStateToLocalStorage } from "../../utils/localStorageFuncion"
import { CART_KEY } from "../../Constants"
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";


const Card = (props) => {

    const {name, price, url, id, color, setChosenItems, isFavourite, toggleFavourite} = props;

    const [header, setHeader] = useState('Do you want to add?');
    const [text, setText] = useState(`By clicking OK, you will add the corresponding product to the cart.`);
    const [modal, setModal] = useState(false);

    const clickToggle = (e) => {
        // e.preventDefault()
        if(!modal) {
          setModal(true)
        } else {
          setModal(false)
        }
    }
    
    const changeColorStar = (e) => {
        e.preventDefault()
        toggleFavourite(id)
    }

    const handleClicktoBtn = (e) => {

        setChosenItems((prev) => {
            const newCardState = [...prev]
            // const item = newCardState.find((item) => item.id === id)
            // if (item) {
            //     console.log(item)
            //     item.count++
            //     return newCardState
            // } else {
            //     return [{name, price, url, id, color, count:1}, ...prev]
            // }

            const index = newCardState.findIndex((item) => item.id === id)
            if (index !== -1) {
                newCardState[index].count++
                saveStateToLocalStorage(CART_KEY, newCardState)
                clickToggle()
                return newCardState
            } else {
                const newState = [{name, price, url, id, color, isFavourite, count:1}, ...prev]
                saveStateToLocalStorage(CART_KEY, newState)
                clickToggle()
                return newState
            }
        })
    }

    return(
        <div className={styles.wrapper}>
            <a href="#" className={styles.card}>
                <div className={styles.favouritesWrap}>
                    {/* <a href="#" className={classNames(styles.favourites, {[styles.active] : isFavourite})} onClick={changeColorStar}>
                        <StarIcon/>
                    </a> */}
                    <div className={classNames(styles.favourites, {[styles.active] : isFavourite})} onClick={changeColorStar}>
                    <StarIcon/>
                    </div>
                </div>
                <img className={styles.img} src={url} alt={name} />
                <p className={styles.caption}>{name}</p>
                <div className={styles.infoWrap}>
                    <span className={styles.info}>your price: {price} $</span>
                    <span className={styles.desc}> color: {color}</span>
                </div>

                <Button onClick={clickToggle} >Add to cart</Button>

                {modal ? <Modal header={header} text={text} isCloseButton={clickToggle} actions={
                    <>
                        <button className='btnActions' onClick={handleClicktoBtn}>Ok</button>
                        <button className='btnActions' onClick={clickToggle}>Cancel</button>
                    </>
                } closefunction={clickToggle}/> : null}

            </a>
        </div>
    )
}

Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string,
    id: PropTypes.number,
    color: PropTypes.string,
    setChosenItems: PropTypes.func,
    isFavourite: PropTypes.bool,
    toggleFavourite: PropTypes.func,
}

Card.defaultProps = {
    name: 'text',
    price: 'text',
    url: 'text',
    id: 0,
    color: 'text',
    setChosenItems: () => {},
    isFavourite: 'false',
    toggleFavourite: () => {},
}

export default memo(Card);



