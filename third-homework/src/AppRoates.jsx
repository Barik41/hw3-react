import React from "react";
import { Routes, Route } from 'react-router-dom';
import HomePage from "./pages/Home/HomePage";
import Favourite from "./pages/Favourite/Favourire";
import Choosen from "./pages/Choosen/Choosen";

const AppRoutes = (props) => {

    const { items, itemsChoosen, setChosenItems, toggleFavourite} = props;

    return(
        <Routes>
            <Route path='/' element={ <HomePage items={items} itemsChoosen={itemsChoosen} setChosenItems={setChosenItems} toggleFavourite={toggleFavourite} />} />
            <Route path='/favourite' element={ <Favourite items={items} setChosenItems={setChosenItems} toggleFavourite={toggleFavourite}/>} />
            <Route path='/choosen' element={ <Choosen itemsChoosen={itemsChoosen} setChosenItems={setChosenItems} />} />
        </Routes>
    )
}

export default AppRoutes;