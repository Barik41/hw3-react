import React from "react";
import ItemsContainer from "../../components/ItemsContainer/itemsContainer";
import ChoosenItems from "../../components/ChoosenItems/ChoosenItems";

const HomePage = (props) => {

    const { items, itemsChoosen, setChosenItems, toggleFavourite} = props;

    return(
        <div className="container">
            <ItemsContainer items={items} setChosenItems={setChosenItems} toggleFavourite={toggleFavourite}/>
            <ChoosenItems items={itemsChoosen} setChosenItems={setChosenItems}/>
        </div>
    )
}

export default HomePage;