import React  from "react";
import FavouriteContainer from "../../components/FavouriteContainer/FavouriteContainer";

const Favourite = (props) => {

    const { items, setChosenItems, toggleFavourite} = props;


    // const [favState, setFavState] = useState(items)
    // console.log(favState)

    // ((prev) => {
    //     const newFav = [...prev]
    //     console.log(newFav)
    // })()

    return(
        <div className="container">
            <FavouriteContainer items={items} setChosenItems={setChosenItems} toggleFavourite={toggleFavourite}/>
        </div>
    )
}

export default Favourite;