import React from "react";
import ChoosenItems from "../../components/ChoosenItems/ChoosenItems";

const Choosen = (props) => {
    const {itemsChoosen, setChosenItems} = props;
    
console.log(setChosenItems)

    return(
        <div className="container">
            <ChoosenItems items={itemsChoosen} setChosenItems={setChosenItems}/>
        </div>
    )
}

export default Choosen;