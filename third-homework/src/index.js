import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';

import { ErrorBoundary } from "react-error-boundary";
import SomethingWentWrong from './components/SomethingWentWrong/SomethingWentWrong';
import { BrowserRouter } from 'react-router-dom';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <ErrorBoundary fallback={<SomethingWentWrong />}>
      <BrowserRouter>
        <App />
        </BrowserRouter>
    </ErrorBoundary>

);